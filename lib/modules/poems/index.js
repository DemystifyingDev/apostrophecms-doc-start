module.exports = {
  name: "poem",
  extend: "apostrophe-pieces",
  label: "Poem",
  pluralLabel: "Poems",
  addFields: [
    {
      name: "title",
      label: "Title",
      type: "string",
      required: true,
    },
    {
      name: "text",
      label: "Text",
      type: "area",
      options: {
        widgets: {
          "apostrophe-rich-text": {
            toolbar: ["Bold", "Italic", "Link", "Unlink"],
          },
          "apostrophe-images": {},
        },
      },
    },
    {
      name: "date",
      label: "Date",
      type: "date",
    },
  ],
};
