module.exports = {
  name: "poet",
  extend: "apostrophe-pieces",
  label: "Poet",
  pluralLabel: "Poets",
  addFields: [
    {
      name: "title",
      label: "Full Name",
      type: "string",
      required: true,
      contextual: true,
    },
    {
      name: "firstName",
      label: "First Name",
      type: "string",
      required: true,
    },
    {
      name: "lastName",
      label: "Last Name",
      type: "string",
      required: true,
    },
    {
      name: "bio",
      label: "Biography",
      type: "area",
      options: {
        widgets: {
          "apostrophe-rich-text": {
            toolbar: ["Bold", "Italic", "Link", "Unlink"],
          },
          "apostrophe-images": {},
        },
      },
    },
    {
      name: "phone",
      label: "Phone",
      type: "string",
    },
    {
      name: "thumbnail",
      label: "Thumbnail",
      type: "singleton",
      widgetType: "apostrophe-images",
      options: {
        limit: 1,
        minSize: [200, 200],
        aspectRatio: [1, 1],
      },
    },
    {
      // Join field names MUST start with _
      name: "_poems",
      label: "Poems",
      type: "joinByArray",
      // SINGULAR, to match the `name` option, not the module name
      withType: "poem",
    },
  ],
  arrangeFields: [
    {
      name: "contact",
      label: "Contact",
      fields: ["firstName", "lastName", "phone"],
    },
    {
      name: "admin",
      label: "Administrative",
      fields: ["slug", "published", "tags"],
    },
    {
      name: "content",
      label: "Content",
      fields: ["thumbnail", "bio", "_poems"],
    },
  ],
  construct: function (self, options) {
    self.beforeSave = function (req, piece, options, callback) {
      piece.title = piece.firstName + " " + piece.lastName;
      return callback();
    };
  },
};
