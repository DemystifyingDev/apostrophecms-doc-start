module.exports = {
  extend: "apostrophe-pieces-widgets",
  label: "Poem Widget",
  filters: {
    projection: {
      title: 1,
      text: 1,
      date: 1,
      // Not a real database property, but including it in the projection
      // fetches everything needed to populate it
      _url: 1,
    },
  },
  addFields: [],
};
